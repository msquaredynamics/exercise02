# README #

This demo app demonstrated how to navigate between activities, using the startActivity() and startActivityForResult() methods.
It also demonstrates how to save/restore the UI using the savedInstanceState Bundle.
