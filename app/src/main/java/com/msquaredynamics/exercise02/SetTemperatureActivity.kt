package com.msquaredynamics.exercise02

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_set_temperature.*

class SetTemperatureActivity : AppCompatActivity(), View.OnClickListener {

    private var setTemperature = 0


    /*
     * Implementation of the OnSeekBarChangeListener.
     * Since we are implementing an interface, all its methods must be implemented,
     * even if they are empty!
     */
    private val seekBarListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
            setTemperature = p1

            // The string resources expects a decimal number as parameter. See strings.xml
            // Parameters can be passed after the resource id using getString(), in the same order as they are delcared
            // in the string resource in the xml file
            val str = getString(R.string.activity_set_temperature_actual_setpoint_temperature, setTemperature)

            txtview_activity_set_temperature.text = str
        }

        override fun onStartTrackingTouch(p0: SeekBar?) { }

        override fun onStopTrackingTouch(p0: SeekBar?) { }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_temperature)

        button_activity_set_temperature_confirm.setOnClickListener(this)
        button_activity_set_temperature_cancel.setOnClickListener(this)

        /* We only want to read the Intent the first time the activity is created, not when the screen orientation
         * changes.
         */
        if (savedInstanceState == null) {
            setTemperature = intent.getIntExtra("temperature", 0)
            seekbar_activity_set_temperature.progress = setTemperature
            txtview_activity_set_temperature.text =
                    getString(R.string.activity_set_temperature_actual_setpoint_temperature, setTemperature)
        }

        seekbar_activity_set_temperature.setOnSeekBarChangeListener(seekBarListener)

    }


    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        if (setTemperature == 0) {
            /* OnSeekBarChangeListener will NOT be notified if the screen orientation changes and the temperature value set is 0.
             * This happens because when the SeekBar is recreated after the orientation changed, its default value is
             * already 0, so the framework doesn't need to change the progress to restore the UI of this component. Hence,
             * the OnSeekBarChangeListener will not receive any call. As a consequence, we need to manually write the
             * value of the TextView
             */
            txtview_activity_set_temperature.text = getString(R.string.activity_set_temperature_actual_setpoint_temperature, setTemperature)
        }
    }



    override fun onClick(p0: View?) {
        when (p0?.id) {
            button_activity_set_temperature_confirm.id -> {
                setResult(Activity.RESULT_OK, Intent().apply {
                    putExtra("temperature", setTemperature)
                })
            }

            button_activity_set_temperature_cancel.id -> {
                setResult(Activity.RESULT_CANCELED)
            }
        }

        finish()

    }
}
