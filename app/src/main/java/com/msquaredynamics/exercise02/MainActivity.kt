package com.msquaredynamics.exercise02

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    /**
     * This is a class variable. On device orientation changes, the framework will destroy and recreate this class.
     * Therefore, this variable will be lost during the destroy/creation process. To avoid data loss, we can save its
     * value overriding onSaveInatanceState and set it back overriding onRestoreInstanceState() or modifying onCreate()
     * accordingly.
     */
    private var setpointTemperature: Int? = null


    /**
     * Listener for the "Set" temperature button clicks
     */
    private fun buttonClickListener() {
        val intent = Intent(this, SetTemperatureActivity::class.java).apply {
            putExtra("temperature", setpointTemperature)
        }

        startActivityForResult(intent, REQUEST_CODE_SET_TEMPERATURE)
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button_activity_main_set_temperature.setOnClickListener { buttonClickListener() }
        updateTemperature()

        Log.d("MainActivity", "onCreate")
    }

    override fun onResume() {
        super.onResume()
        Log.d("MainActivity", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d("MainActivity", "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d("MainActivity", "onStop")
    }


    override fun onDestroy() {
        super.onDestroy()
        Log.d("MainActivity", "onDestroy")
    }


    override fun onStart() {
        super.onStart()
        Log.d("MainActivity", "onStart")
    }


    /**
     * We override this function to save the value of setpointTemperature
     */
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        setpointTemperature?.apply {
            outState?.putInt("temperature", this)
        }


    }

    /**
     * Here we set the previously saved value of setpointTemperature
     */
    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        val temp = savedInstanceState?.getInt("temperature", -1)
        if (temp != -1) {
            setpointTemperature = temp
            updateTemperature()
        }
    }


    /**
     * Invoked by the framework when an activity previously launched returns
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            REQUEST_CODE_SET_TEMPERATURE -> {
                if (resultCode == Activity.RESULT_OK) {
                    setpointTemperature = data!!.getIntExtra("temperature", 0)
                    updateTemperature()
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    Snackbar.make(constraintlayoyt_activity_main_container, "Cancelled", Snackbar.LENGTH_SHORT).show()
                }
            }
        }
    }


    /**
     * Updates the temperature textview based on the value of the setpointTemperature variable
     */
    private fun updateTemperature() {
        if (setpointTemperature != null) {
            txtview_activity_main_temperature.text = getString(R.string.activity_main_textview_temperature, setpointTemperature)
        } else {
            txtview_activity_main_temperature.text = "N/A"
        }
    }



    companion object {
        // Request code used to launch the SetTemperatureActivity
        private const val REQUEST_CODE_SET_TEMPERATURE = 200
    }

}
